class User < ActiveRecord::Base
has_many :cart_items, :through => :cart
has_many :wish_items, :through => :wishlist
has_many :bids
has_many :products

ratyrate_rater

has_many :products_with_bids, :through => :bids, :source => :product

attr_accessor :country_id
attr_accessor :city_name
belongs_to :city
validates :email, uniqueness: true
validates :name, presence: true
validates :name, uniqueness: true

has_attached_file :avatar, :styles => { :medium => "300x300#", :thumb => "100x100#" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
