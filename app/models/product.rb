class Product < ActiveRecord::Base
ratyrate_rateable "speed"
  has_many :wish_items
  has_many :cart_items
  belongs_to :subcategory
  belongs_to :user
  has_many :bids
  attr_accessor :category_id
  attr_accessor :subcategory_name
  has_many :product_attachments
  accepts_nested_attributes_for :product_attachments

  validates :initial_price, numericality: true
  validates :name, presence: true, length: { maximum: 20 }
  validates :selling_price, numericality: true
  validates :description, presence: true, length: { maximum: 90 }
  #validates :availibility, presence: true, length: { maximum: 30 }

before_destroy :ensure_not_referenced_by_any_cart_item

 private

 def ensure_not_referenced_by_any_wish_item
  if wish_items.count.zero?
   return true
  else
   errors[:base]<< "wish items present"
   return false
   end
  end


 def ensure_not_referenced_by_any_cart_item
  if cart_items.count.zero?
   return true
  else
   errors[:base]<< "cart items present"
   return false
   end
  end


  def self.search_products(params)
    products = Product.order(:name)
    products = products.where("name like ?", "%#{params[:search]}%") if !params[:search].strip.empty?
    products = products.where(subcategory_id: params[:product][:subcategory_id]) if !params[:product][:subcategory_id].strip.empty?
    products
  end
end
