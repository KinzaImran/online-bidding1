class CartItem < ActiveRecord::Base
belongs_to :product
belongs_to :cart


  def total_price
     product.selling_price
  end

end
