# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#
jQuery ->
  cities = $('#user_city_id').html()
  $('#user_country_id').change ->
    country = $('#user_country_id :selected').text()
    options = $(cities).filter("optgroup[label='#{country}']").html()
    if options
      $('#user_city_id').html(options)
    else
      $('#user_city_id').empty()
