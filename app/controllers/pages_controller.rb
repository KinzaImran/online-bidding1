class PagesController < ApplicationController
  def home
  end

  def contact
  end

  def faq
  end

  def blog
  end

  def about
  end

  def zip
  @user = current_user
  @products = @user.products_with_bids
  @products = Product.where(:user_id => current_user.id)
  @bids = Bid.where(:user_id => current_user.id)
  end

   private

   def page_params
      params.require(:page).permit(:cart_id, :wishlist_id).merge(user_id: current_user.id)
    end
end
