class UsersController < ApplicationController
before_filter :authenticate_user!

def show
@user = User.find(params[:id])
@city = City.all
end

def index
@users = User.all
@cities = City.all
end

 def user_params
      params.require(:user).permit(:name, :email,:password,:password_confirmation,:cnic_no,:contact_no,:address,:country_id, :gender, :avatar)
   end

end
