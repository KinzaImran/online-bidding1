class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_filter :configure_permitted_parameters, if: :devise_controller?

    protected

        def configure_permitted_parameters
            devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name,:cnic_no,:contact_no,:is_female, :email, :password, :address,:city_id,:country_id) }
            devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name,:cnic_no,:contact_no,:is_female,:email, :password, :current_password, :address, :city_id,:country_id,:avatar) }
        end

private
  def current_cart
     Cart.find(session[:cart_id])
    rescue ActiveRecord::RecordNotFound
     cart=Cart.create
     session[:cart_id]= cart.id
        cart
        end
  helper_method :current_cart
  
   def current_wishlist
  Wishlist.find(session[:wishlist_id])
  rescue ActiveRecord::RecordNotFound
    wishlist  =  Wishlist.create
session[:wishlist_id] = wishlist.id
       wishlist
  end
  helper_method :current_wishlist

end
