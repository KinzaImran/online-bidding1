class RemoveAvailibilityFromProduct < ActiveRecord::Migration
  def change
    remove_column :products, :availibility, :string
  end
end
