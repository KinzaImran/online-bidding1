class RemoveAvailibilityFromProducts < ActiveRecord::Migration
  def change
    remove_column :products, :availibility, :boolean
  end
end
