class AddWaitForClosingDateToProducts < ActiveRecord::Migration
  def change
    add_column :products, :wait_for_closing_date, :boolean
  end
end
