class AddProductAvailableToProducts < ActiveRecord::Migration
  def change
    add_column :products, :product_available, :boolean
  end
end
