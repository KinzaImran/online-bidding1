class AddNameToUsers < ActiveRecord::Migration
  def change
    add_column :users, :name, :string
    add_column :users, :cnic_no, :integer
    add_column :users, :contact_no, :integer
    add_column :users, :location, :string
    add_column :users, :is_female, :boolean
  end
end
