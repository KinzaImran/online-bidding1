require 'rails_helper'

RSpec.describe "wish_items/index", type: :view do
  before(:each) do
    assign(:wish_items, [
      WishItem.create!(
        :product_id => 1,
        :wishlist_id => 2
      ),
      WishItem.create!(
        :product_id => 1,
        :wishlist_id => 2
      )
    ])
  end

  it "renders a list of wish_items" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
