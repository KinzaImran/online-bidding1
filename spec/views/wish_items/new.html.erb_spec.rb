require 'rails_helper'

RSpec.describe "wish_items/new", type: :view do
  before(:each) do
    assign(:wish_item, WishItem.new(
      :product_id => 1,
      :wishlist_id => 1
    ))
  end

  it "renders new wish_item form" do
    render

    assert_select "form[action=?][method=?]", wish_items_path, "post" do

      assert_select "input#wish_item_product_id[name=?]", "wish_item[product_id]"

      assert_select "input#wish_item_wishlist_id[name=?]", "wish_item[wishlist_id]"
    end
  end
end
