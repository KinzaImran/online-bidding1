require 'rails_helper'

RSpec.describe "wish_items/show", type: :view do
  before(:each) do
    @wish_item = assign(:wish_item, WishItem.create!(
      :product_id => 1,
      :wishlist_id => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
  end
end
